const web = require("./web-scrape.js"); // webスクレイピング機能をインポート
const request = require('request-promise'); //webAPI呼び出しメソッドをインポート
const fs = require('fs');
const encoding = require("encoding-japanese");

var areas = []
var stations = []
const exclude = ["周辺", "周り", "周囲", "お店", "料理", "味", "店", "料理店", "ご飯", "飯"]

//配列の重複を削除する関数
const uniq = (array) => {
    return array.filter((value, index, array) => {
        return array.indexOf(value) === index;
    });
}

//地名・駅名を配列にセットする初期化処理
exports.init = async () => {
    //地名をスクレイピング
    await web.getAreas(areas);

    //駅名を取得
    var buffer = fs.readFileSync('./public/eki.csv');
    stations = encoding.convert(buffer, {
        from: 'SJIS',
        to: 'UNICODE',
        type: 'string',
    }).split('\n');

    stations.forEach((value, index, array) => {
        array[index] = value.replace(/\(.*\)/, '');
    });

    stations = uniq(stations);    
}

// ポストバックアクションによるメッセージオブジェクト格納メソッド
exports.proceedByPostback = async (event, db) => {
    let param = event.postback.data;

    if (param === "count") {
        return [
            {
                "type": "template",
                "altText": "This is a buttons template",
                "template": {
                    "type": "buttons",
                    "text": "表示件数を選択してね！",
                    "actions": [
                        {
                            "type": "postback",
                            "label": "5",
                            "data": "count:5"
                        },
                        {
                            "type": "postback",
                            "label": "10",
                            "data": "count:10"
                        },
                        {
                            "type": "postback",
                            "label": "15",
                            "data": "count:15"
                        }, {
                            "type": "postback",
                            "label": "20",
                            "data": "count:20"
                        }
                    ]
                }
            }
        ]
    }

    if (param === "lower" || param === "upper") {
        let text;
        if (param === "lower") text = "下限";
        else text = "上限";
        return [
            {
                "type": "template",
                "altText": "This is a buttons template",
                "template": {
                    "type": "buttons",
                    "text": "予算の" + text + "の範囲を選択してね！",
                    "actions": [
                        {
                            "type": "postback",
                            "label": "1,000 ~ 4,000",
                            "data": param + "_pre:0"
                        },
                        {
                            "type": "postback",
                            "label": "5,000 ~ 10,000",
                            "data": param + "_pre:1"
                        },
                        {
                            "type": "postback",
                            "label": "15,000 ~ 40,000",
                            "data": param + "_pre:2"
                        }, {
                            "type": "postback",
                            "label": "50,000 ~ 10,0000",
                            "data": param + "_pre:3"
                        }
                    ]
                }
            }
        ]
    }

    if ((param.indexOf("lower_pre:") !== -1) || (param.indexOf("upper_pre:") !== -1)) {
        let params = param.split("_pre:");
        let prices = ["1,000", "2,000", "3,000", "4,000", "5,000", "6,000", "8,000", "10,000", "15,000", "20,000", "30,000", "40,000", "50,000", "60,000", "80,000", "100,000"];
        let index = Number(params[1]) * 4;
        let text;
        if (params[0] === "lower") text = "下限";
        else text = "上限";
        return [
            {
                "type": "template",
                "altText": "This is a buttons template",
                "template": {
                    "type": "buttons",
                    "text": "予算の" + text + "を選択してね！",
                    "actions": [
                        {
                            "type": "postback",
                            "label": prices[index],
                            "data": params[0] + ":" + (index + 1)
                        },
                        {
                            "type": "postback",
                            "label": prices[index + 1],
                            "data": params[0] + ":" + (index + 2)
                        },
                        {
                            "type": "postback",
                            "label": prices[index + 2],
                            "data": params[0] + ":" + (index + 3)
                        }, {
                            "type": "postback",
                            "label": prices[index + 3],
                            "data": params[0] + ":" + (index + 4)
                        }
                    ]
                }
            }
        ];
    }

    if (param === "keyword") {
        return [
            {
                "type": "text",
                "text": "次のように入力してね！\r\n（例）「東京駅周辺の美味しい肉料理の居酒屋」\r\n位置情報を送信すると、周辺のお店を教えてあげるよ！"
            }
        ];
    }

    try {
        if (param.indexOf("count:") !== -1) {
            let userId = event.source.userId;
            await db.setCount(userId, Number(param.split(":")[1]));
            await db.setTime(userId);
            return [
                {
                    "type": "text",
                    "text": "表示件数を設定したよ！（全ての設定項目は最後の設定から5分後、リセットされます。）"
                }
            ];
        }

        if (param.indexOf("lower:") !== -1) {
            let userId = event.source.userId;
            await db.setLower(userId, param.split(":")[1]);
            await db.setTime(userId);
            return [
                {
                    "type": "text",
                    "text": "下限を設定したよ！（全ての設定項目は最後の設定から5分後、リセットされます。）"
                }
            ];
        }

        if (param.indexOf("upper:") !== -1) {
            let userId = event.source.userId;
            await db.setUpper(userId, param.split(":")[1]);
            await db.setTime(userId);
            return [
                {
                    "type": "text",
                    "text": "上限を設定したよ！（全ての設定項目は最後の設定から5分後、リセットされます。）"
                }
            ];
        }

        if (param === "reset") {
            await db.deleteConfig(event.source.userId);
            return [
                {
                    "type": "text",
                    "text": "設定をリセットしました。"
                }
            ];
        }
    } catch (err) {
        throw err;
    }
}



// メッセージアクションによるメッセージオブジェクト格納メソッド
exports.proceedByMessage = async (event, db) => {

    let responseMessages = [];

    let places = [];
    let keywords = [];

    try {
        //テキストから、名詞を抜き出し、地名（駅名） or キーワードを設定
        if (event.message.type === "text") {

            await request.get({
                uri: "https://mecab-web-api.herokuapp.com/v1/parse?sentence=" + encodeURIComponent(event.message.text) + "&nbest_num=1",
            }, (err, req, res) => {
                if (err !== null) throw err;
                let words = JSON.parse(res).items[0].words;
                words.forEach((word) => {
                    let surface = word.surface
                    if (word.pos !== '名詞') return;
                    if (exclude.includes(surface)) return;

                    if (areas.includes(surface) || stations.includes(surface)) {
                        places.push(surface);
                        return;
                    }

                    keywords.push(surface);
                });
            });
        }
        //入力が位置情報の場合、位置情報から町名を取得し、変数に格納
        else if (event.message.type === "location") {
            //位置情報から郵便番号を抜き出す
            let address = event.message.address;
            let postNumber = address.substr(address.indexOf("〒") + 1, 8).replace('-', '');
            await request.get({
                uri: "http://zipcloud.ibsnet.co.jp/api/search?zipcode=" + postNumber,
            }, (err, req, res) => {
                if (err !== null) throw err;
                let address = JSON.parse(res).results[0];
                places.push(address.address3 + "(" + address.address1);
            });
        } else return [{
            type: "text",
            text: "文章か位置情報を送信してね！"
        }];

        //DBから、設定情報（サイト返却個数）取得メソッドの呼び出し
        const config = await db.getConfig(event.source.userId);

        //重複を削除
        places = uniq(places);
        keywords = uniq(keywords);

        //地名配列とキーワード配列を文字列に変換
        let placeStr = ""
        places.forEach((place) => {
            placeStr += place + '、';
        });
        placeStr = placeStr.slice(0, -1);
        let keywordStr = ""
        keywords.forEach((keyword) => {
            keywordStr += keyword + '、';
        });
        keywordStr = keywordStr.slice(0, -1);

        //食べログ検索結果URL取得メソッドの呼び出し
        const urls = await web.getUrls(placeStr, keywordStr, config);
        if (urls.length === 0) {
            responseMessages.push({
                type: "text",
                text: "検索結果がなかったよ！"
            });
        } else {
            //結果URLを応用メッセージ用配列に格納
            urls.forEach((url) => {
                responseMessages.push({
                    type: "text",
                    text: url
                })
            });
        }
    } catch (err) {
        throw err;
    }
    return responseMessages;
}
