exports.init = async function (bot) {
    const path = require('path');
    const fs = require('fs');

    // ------------------------------------------------------------------------------
    // リッチメニュー設定を作成
    const richMenu = {
        size: {
            width: 800,
            height: 540
        },
        selected: true,
        name: "Nice richmenu",
        chatBarText: "設定変更",
        areas: [
            {
                bounds: {
                    x: 0,
                    y: 0,
                    width: 267,
                    height: 270
                },
                action: {
                    type: "postback",
                    data: "count"
                }
            },
            {
                bounds: {
                    x: 268,
                    y: 0,
                    width: 267,
                    height: 270
                },
                action: {
                    type: "postback",
                    data: "lower"
                }
            },
            {
                bounds: {
                    x: 535,
                    y: 0,
                    width: 266,
                    height: 270
                },
                action: {
                    type: "postback",
                    data: "upper"
                }
            },
            {
                bounds: {
                    x: 0,
                    y: 271,
                    width: 400,
                    height: 270
                },
                action: {
                    type: "postback",
                    data: "keyword"
                }
            },
            {
                bounds: {
                    x: 401,
                    y: 271,
                    width: 400,
                    height: 270
                },
                action: {
                    type: "postback",
                    data: "reset"
                }
            }
        ]
    };

    // リッチメニューをデフォルトに設定
    try {
        const richMenuId = await bot.createRichMenu(richMenu);
        await bot.setRichMenuImage(richMenuId, fs.createReadStream(path.resolve("public/button-init.png")));
        await bot.setDefaultRichMenu(richMenuId);
    } catch (err) {
        throw err;
    }
    return;
}
