exports.connect = () => {
    let db;

    const admin = require("firebase-admin");

    admin.initializeApp({
        credential: admin.credential.cert({
            type: process.env.TYPE,
            project_id: process.env.PROJECT_ID,
            private_key_id: process.env.PRIVATE_KEY_ID,
            private_key: process.env.PRIVATE_KEY.replace(/\\n/g, '\n'),
            client_email: process.env.CLIENT_EMAIL,
            client_id: process.env.CLIENT_ID,
            auth_uri: process.env.AUTH_URI,
            token_uri: process.env.TOKEN_URI,
            auth_provider_x509_cert_url: process.env.AUTH_PROVIDER_X509_CERT_URL,
            client_x509_cert_url: process.env.CLIENT_X509_CERT_URL

        }),
        databaseURL: "https://taberogu-line-bot.firebaseio.com"
    });

    db = admin.firestore();
    const settings = { timestampsInSnapshots: true };
    db.settings(settings);

    return {
        db: db,
        getConfig: async function (docId) {
            let doc = this.db.collection(`users`).doc(docId);
            return await doc.get()
                .then((doc) => {
                    return doc.data();
                }).catch((err) => {
                    throw err;
                });

        },
        getTime: async function (docId) {
            return await this.db.collection('users').doc(docId).get()
                .then((doc) => {
                   if(!doc.data()) return null;
                   return doc.data().time;
                }).catch((err) => {
                    throw err;
                });
        },
        setCount: async function (docId, count) {
            return await this.db.collection('users').doc(docId).set({
                count: count
            }, { merge: true })
                .catch((err) => {
                    throw err;
                });
        },
        setLower: async function (docId, lower) {
            return await this.db.collection('users').doc(docId).set({
                lower: lower
            }, { merge: true })
                .catch((err) => {
                    throw err;
                });
        },
        setUpper: async function (docId, upper) {
            return await this.db.collection('users').doc(docId).set({
                upper: upper
            }, { merge: true })
                .catch((err) => {
                    throw err;
                });
        },
        setTime: async function (docId) {
            return await this.db.collection('users').doc(docId).set({
                time: new Date()
            }, { merge: true })
                .catch((err) => {
                    throw err;
                });
        },
        deleteConfig: async function (docId) {
            return await this.db.collection('users').doc(docId).delete()
                .catch((err) => {
                    throw err;
                });
        }
    };
}