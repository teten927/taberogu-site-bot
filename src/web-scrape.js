const client = require('cheerio-httpcli');

//日本の全地名を取得するメソッド
exports.getAreas = async (areas) => {
    const placePage = await client.fetch("https://www.mlit.go.jp/road/sign/sign/annai/6-hyou-timei.htm");
    await placePage.$("table td[bgcolor='#ffff66'] p").prev().prevObject.each((idx, obj) => {areas = areas.concat(obj.children[0].data.split('、'))});
    await placePage.$("table td[bgcolor='#ffffcc'] p").prev().prevObject.each((idx, obj) => {areas = areas.concat(obj.children[0].data.split('、'))});
    return areas;
}

//店のURLを取得するメソッド
exports.getUrls = async (place, keyword, config) => {
    let count, lower, upper;

    if (config !== undefined){
        if(config.count) count = config.count;
        if (config.lower) lower = config.lower;
        else lower = "0";
        if (config.upper) upper = config.upper;
        else upper = "0";
    }



    let urls = [];

    try {
        const result1 = await client.fetch('https://tabelog.com');
        const result2 = await result1.$('form[name]').submit({ sa: place, sk: keyword });
        if (result2.$("span[class='list-condition__count']").text() === "0") return urls;
        if (result2.$("strong[class='list-condition__title']").text() === "全国のお店、レストラン") return urls;

        let result3 = await client.fetch(result2.$("a[class='navi-rstlst__label navi-rstlst__label--rank']").attr('href'));

        if (!(lower === "0" && upper === "0")) {
            result3.$("#lstcos-sidebar").val(lower);
            result3.$("#lstcost-sidebar").val(upper);
            result3 = await result3.$("button[name='commit']").click();
        }

        result3.$("a[class='list-rst__rst-name-target cpy-rst-name js-ranking-num']").each((idx, obj) => {
            if (idx > (count - 1 || 2)) return;
            urls.push(obj.attribs.href);
        });
    } catch (err) {
        throw err;
    }
    return urls;
}