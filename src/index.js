// -----------------------------------------------------------------------------
// モジュールのインポート
const server = require("express")();
const line = require("@line/bot-sdk"); // Messaging APIのSDKをインポート
const cron = require('node-cron');
const proceedMessage = require("./proceedMessage.js");
const dbObj = require("./db-access.js");
const richMenu = require("./rich-menu.js");

var userIdList = [];

try {
    // -----------------------------------------------------------------------------
    // パラメータ設定
    var lineConfig = {
        channelAccessToken: process.env.LINE_ACCESS_TOKEN, // 環境変数からアクセストークンをセットしています
        channelSecret: process.env.LINE_CHANNEL_SECRET // 環境変数からChannel Secretをセットしています
    };

    // ------------------------------------------------------------------------------
    // APIコールのためのクライアントインスタンスを作成
    var bot = new line.Client(lineConfig);

    // ------------------------------------------------------------------------------
    // リッチメニュー初期動作メソッド
    richMenu.init(bot).catch(err => console.error(err));

    // -----------------------------------------------------------------------------
    // DB接続
    var db = dbObj.connect();

    
    // -----------------------------------------------------------------------------
    // 5分おきにDBのごみ削除
    cron.schedule('0 */5 * * * *', () => {
        let tmpUserIdList = [];
        let now = new Date();
        userIdList.forEach(async userId => {
            let getedTime = await db.getTime(userId);
            if(getedTime === null) return;
            let time = new Date(getedTime._seconds*1000 + getedTime._nanoseconds/100000);
            if ((now - time)/(1000*60) >= 5) await db.deleteConfig(userId);
            else tmpUserIdList.push(userId);
        });
        userIdList = tmpUserIdList;
    });
    
    // -----------------------------------------------------------------------------
    // 配列初期化処理
    proceedMessage.init();

    // -----------------------------------------------------------------------------
    // Webサーバー設定
    server.listen(process.env.PORT || 3000);

} catch (err) {
    console.error(err);
}

// -----------------------------------------------------------------------------
// ルーター設定
server.post('/bot', line.middleware(lineConfig), (req, res, next) => {
    // 先行してLINE側にステータスコード200でレスポンス
    res.sendStatus(200);

    //デバッグ用：リクエストボディの出力
    console.log(req.body);
    

    // イベントオブジェクトを順次処理
    req.body.events.forEach(async event => {
        let userId = event.source.userId;
        if (userIdList.indexOf(userId) === -1) userIdList.push(userId);

        try {
            var responseMessages;
            if (event.type === "postback") {
                responseMessages = await proceedMessage.proceedByPostback(event, db);
            } else if (event.type === "message") {
                await bot.replyMessage(event.replyToken, [{
                    type: "text",
                    text: "検索中だよ！"
                }]);
                // メッセージアクション実行メソッドを処理し、返答メッセージ配列に格納
                responseMessages = await proceedMessage.proceedByMessage(event, db);
            } else {
                responseMessages.push({
                    type: "text",
                    text: "送信メッセージの形式が違うよ！"
                });
            }
        } catch (err) {
            responseMessages = [];
            console.error(err);
            responseMessages.push({
                type: "text",
                text: "システムエラーのため、処理できませんでした。管理者にクレームを言ってください。"
            })
        } finally {
            responseMessages.forEach(async responseMessage => {
                await bot.pushMessage(event.source.userId, responseMessage)
                    .catch((err) => console.error(err));
            });
        }
    });
});
